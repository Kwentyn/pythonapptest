from flask import Flask, render_template, request, url_for, flash, redirect  # ImportFlaslk
from werkzeug.exceptions import abort  # 404 Page
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy

# init flask app instance
app = Flask(__name__)

# app Config =>  DATABASE URI
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://jdrvlqnyeddyuc:1018514fe912b7558ac0210f57fa5a07bcf34979a22a490e9aeb96ce94273127@ec2-52-30-81-192.eu-west-1.compute.amazonaws.com:5432/d9hgh5c16qhj12"

db = SQLAlchemy(app)


class Cours(db.Model):
    __tablename__ = "cours"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String())
    content = db.Column(db.String())

    def __init__(self, id, title, content):
        self.title = title
        self.content = content
        self.id = id


def get_post(post_id):
    post = db.session.query(Cours).filter_by(id=post_id).first()
    return post


# App Def
@app.route("/")
def main():
    cours = db.session.execute('SELECT * FROM cours').fetchall()
    return render_template('index.html', posts=cours)


@app.route('/<int:post_id>')
def post(post_id):
    post = get_post(post_id)
    if post is None:
        abort(404)
    return render_template('post.html', post=post)


@app.route('/create', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        data = request.form
        v_title = data['title']
        v_content = data['content']

        if not v_title:
            return redirect(url_for('main'))
            # flash('Title is required!')
        else:
            lastObject = db.session.query(Cours).order_by(sqlalchemy.desc(Cours.id)).first()
            try:
                maxIncr = lastObject.id + 1
            except Exception:
                maxIncr = 0
            new_lesson = Cours(maxIncr, v_title, v_content)
            db.session.add(new_lesson)
            db.session.commit()
            return redirect(url_for('main'))

    return render_template('create.html')


@app.route('/<int:id>/edit', methods=('GET', 'POST'))
def edit(id):
    post = get_post(id)

    if request.method == 'POST':
        data = request.form
        v_title = data['title']
        v_content = data['content']

        if not v_title:
            flash('Title is required!')
        else:
            updated_lesson = db.session.query(Cours).filter_by(id=id).first()
            updated_lesson.title = v_title
            db.session.commit()
            updated_lesson = db.session.query(Cours).filter_by(id=id).first()
            updated_lesson.content = v_content
            db.session.commit()

            return redirect(url_for('main'))

    return render_template('edit.html', post=post)


@app.route('/<int:id>/delete', methods=('POST',))
def delete(id):
    deleted_lesson = db.session.query(Cours).filter_by(id=id).first()
    db.session.delete(deleted_lesson)
    db.session.commit()
    # flash('"{}" was successfully deleted!'.format(post['title']))
    return redirect(url_for('main'))


if __name__ == "__main__":
    app.run()
