from urllib.parse import urlencode
import json
import sqlite3
import pytest




def capital_case(x):
    return x.capitalize()
    
def test_capital_case():
    assert capital_case('semaphore') == 'Semaphore'

@pytest.fixture()
def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.execute('CREATE TABLE test (title TEXT, content TEXT)')
    return conn

def test_sqlite(get_db_connection):
    get_db_connection.execute('INSERT INTO test (title, content) VALUES ("title1", "content1"),("title2", "content2")')

    # test 1
    c = get_db_connection.execute('SELECT * FROM test;')
    assert list(c) == [('title1', 'content1'),('title2','content2')]

    # test title
    getTitle = get_db_connection.execute('SELECT * FROM test WHERE title = "title1";')
    assert list(getTitle) == [('title1', 'content1')]


    # test content
    getTitle = get_db_connection.execute('SELECT content FROM test;')
    assert list(getTitle) == [('content1',), ('content2',)]



